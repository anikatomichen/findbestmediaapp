package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CreateConnection {
    Connection con = null;

    public Connection getConnection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/nikka db?useSSL=False", "root", "root");
            return (con);


        } catch (ClassNotFoundException e) {
            System.out.println("connection class not found");
        } catch (SQLException e) {
           System.out.println("SQL Exception");
        }
        return (con);
    }
}
