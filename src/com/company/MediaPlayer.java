package com.company;

public class MediaPlayer {

    public void findBestApp(String NameOfFile){
        String extention, fileType;

        ExtentionOfFile fileName = new ExtentionOfFile();
        extention = fileName.getExtentionOfFile(NameOfFile);

        TypeOfFile getTypeOfFile = new TypeOfFile();
        fileType = getTypeOfFile.compareAudioVideo(extention);

        switch (fileType) {
            case "Audio":
                SuggestionsTables getSuggestionsAudio = new AudioPlayerNames();
                getSuggestionsAudio.valuesFromDB();
                getSuggestionsAudio.display();
                break;

            case "Video":
                SuggestionsTables getSuggestionsVideo = new VideoPlayerNames();
                getSuggestionsVideo.valuesFromDB();
                getSuggestionsVideo.display();
                System.out.println("\nCan't play this in office");
                break;

            default:
                System.out.println("This is not mp3 or mp4 extention");

        }
    }
}
