package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class VideoPlayerNames extends SuggestionsTables {
    ResultSet rs;
    ArrayList<String> videoPlayerNames = new ArrayList<String>(20);
    private String temp;
    private String query1;

    public VideoPlayerNames(){
        this.query1="SELECT PlayerName FROM MediaPlayer WHERE FileType='mp4'";
    }

    public void valuesFromDB()throws NullPointerException{
       try {
            CreateConnection conn = new CreateConnection();
            Connection c = conn.getConnection();
            Statement st = c.createStatement();
            rs = st.executeQuery(query1);
            {
                while (rs.next()) {
                    temp = rs.getString(1);
                    videoPlayerNames.add(temp);
                }
            }

            c.close();
        } catch (SQLException e) {
            System.out.println(e);
       }

    }

    public void display(){
        for(String PlayerNames:videoPlayerNames){
            System.out.println(PlayerNames);
        }
    }
}
